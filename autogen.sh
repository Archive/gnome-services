#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gnome-services"

(test -f idl/GNOME_Services_Server.idl) || {
	echo "You must run this script in the top-level gnome services directory"
	exit 1
}

USE_GNOME2_MACROS=1 . gnome-autogen.sh

