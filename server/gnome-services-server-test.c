#include "GNOME_Services_Server.h"
#include <libbonobo.h>

int main (int argc, char *argv[]) {
  CORBA_Environment ev;
  GNOME_Services_Server corba_foo;

  bonobo_init (&argc, argv);
  
  CORBA_exception_init (&ev);

  //  corba_foo = bonobo_get_object  ("OAFIID:GNOME_Services", "IDL:GNOME/Services:1.0", &ev);
  
  printf ("performing bonobo_activation\n");

  corba_foo = (GNOME_Services_Server) bonobo_activation_activate ("repo_ids.has ('IDL:GNOME/Services/Server:1.0')",
			      NULL, 0, NULL, &ev);

  printf ("done doing activation\n");
  g_assert (corba_foo);
  CORBA_exception_init (&ev);
  GNOME_Services_Server_activateService (corba_foo, "MyService", &ev);
}

