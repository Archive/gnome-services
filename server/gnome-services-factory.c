#include <sys/types.h>
#include <glib.h>

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-generic-factory.h>

#include "libgnome-services.h"
#include "gnome-services-server.h"

static BonoboObject *services_server = NULL;

BonoboObject * 
services_server_factory (BonoboGenericFactory *factory,
		  const char           *component_id,
		  gpointer              closure)
{
  printf ("Factory called...");
  if (!services_server) {
    printf ("creating new object!\n");
    services_server = g_object_new (GNOME_TYPE_SERVICES_SERVER, NULL);
  } else {
    printf ("recycling old object\n");
  }

  return services_server;
}

BONOBO_ACTIVATION_FACTORY ("OAFIID:GNOME_Services_Server_Factory",
			   "Services component factory", "1.0",
			   services_server_factory, NULL);
