#ifndef _LIBSERVICE_H_
#define _LIBSERVICE_H_

#include <bonobo-activation/Bonobo_Activation_types.h>

gboolean activate_service (const Bonobo_ActivationID aid);

#endif
